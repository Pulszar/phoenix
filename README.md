<h1>Phoenix is a simple mp3 player</h1>

<p align="center">
  <img src="Image_Of_Player.png" width="600" title="Image of the player">
</p

The main purpose of the project is to show some of the functionality of Java Fx.
Inside you can see an FXML file which is used to create the layout. <br> You can also see how to add a CSS file with CSS style for the UI components.

The main class is <b>phoenix.player.PhoenixStart</b>.
In the project the UI components are injected in <b>phoenix.player.StageController</b>

Maven plugins are used in the <b>pom.xml</b>:
<b>maven-compiler-plugin</b> -> to overwrite the default compiler of Maven to compile java 11 modules
<b>javafx-maven-plugin</b> -> to run the application and build the image (Bundle with all necessary components)


To run the application please execute Maven command:
<b>mvn clean install javafx:run</b>

Or create inside Eclipse Maven build:
<b>clean install javafx:run</b>


To create new image please execute Maven command:
<b>mvn clean install javafx:jlink</b>

Or create inside Eclipse Maven build:
<b>clean install javafx:jlink</b>


To work on this project you need JDK 11 and Maven.
The project is developed on Eclipse IDE so it should be easiest to be imported there.



In the folder releases you can find already build application. For the current moment there are two 64 bit builds.
Ubuntu build is created with Amazon Corretto 11 due to ability to make the build really small. With Ubuntu's Open JDK the build was bigger then 300 mb. Windows build was performed with AdoptOpenJDK 11.

Ubuntu build tested on Ubuntu 18.04 and Windows build tested on Windows 10.
Happy listening to music.


