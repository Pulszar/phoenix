/**
 * 
 */
/**
 * @author natasha
 *
 */
module phoenix
{
	exports phoenix.player.models;
	exports phoenix.player;

	requires java.base;
	requires java.naming;
	requires transitive javafx.base;
	requires transitive javafx.controls;
	requires transitive javafx.fxml;
	requires transitive javafx.graphics;
	requires transitive javafx.media;
	
	requires com.fasterxml.jackson.annotation;
	requires java.net.http;
	requires com.fasterxml.jackson.databind;
	requires com.fasterxml.jackson.core;
	requires jdk.crypto.ec;
	
	opens phoenix.player to javafx.fxml;
}