package phoenix.player.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "changeuuid", "stationuuid", "name", "url", "url_resolved", "homepage", "favicon", "tags", "country", "countrycode",
		"state", "language", "votes", "lastchangetime", "codec", "bitrate", "hls", "lastcheckok", "lastchecktime", "lastcheckoktime",
		"lastlocalchecktime", "clicktimestamp", "clickcount", "clicktrend" })
public class RadioModel
{

	@JsonProperty("changeuuid")
	private String changeuuid;
	@JsonProperty("stationuuid")
	private String stationuuid;
	@JsonProperty("name")
	private String name;
	@JsonProperty("url")
	private String url;
	@JsonProperty("url_resolved")
	private String urlResolved;
	@JsonProperty("homepage")
	private String homepage;
	@JsonProperty("favicon")
	private String favicon;
	@JsonProperty("tags")
	private String tags;
	@JsonProperty("country")
	private String country;
	@JsonProperty("countrycode")
	private String countrycode;
	@JsonProperty("state")
	private String state;
	@JsonProperty("language")
	private String language;
	@JsonProperty("votes")
	private int votes;
	@JsonProperty("lastchangetime")
	private String lastchangetime;
	@JsonProperty("codec")
	private String codec;
	@JsonProperty("bitrate")
	private int bitrate;
	@JsonProperty("hls")
	private int hls;
	@JsonProperty("lastcheckok")
	private int lastcheckok;
	@JsonProperty("lastchecktime")
	private String lastchecktime;
	@JsonProperty("lastcheckoktime")
	private String lastcheckoktime;
	@JsonProperty("lastlocalchecktime")
	private String lastlocalchecktime;
	@JsonProperty("clicktimestamp")
	private String clicktimestamp;
	@JsonProperty("clickcount")
	private int clickcount;
	@JsonProperty("clicktrend")
	private int clicktrend;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("changeuuid")
	public String getChangeuuid()
	{
		return changeuuid;
	}

	@JsonProperty("changeuuid")
	public void setChangeuuid(String changeuuid)
	{
		this.changeuuid = changeuuid;
	}

	@JsonProperty("stationuuid")
	public String getStationuuid()
	{
		return stationuuid;
	}

	@JsonProperty("stationuuid")
	public void setStationuuid(String stationuuid)
	{
		this.stationuuid = stationuuid;
	}

	@JsonProperty("name")
	public String getName()
	{
		return name;
	}

	@JsonProperty("name")
	public void setName(String name)
	{
		this.name = name;
	}

	@JsonProperty("url")
	public String getUrl()
	{
		return url;
	}

	@JsonProperty("url")
	public void setUrl(String url)
	{
		this.url = url;
	}

	@JsonProperty("url_resolved")
	public String getUrlResolved()
	{
		return urlResolved;
	}

	@JsonProperty("url_resolved")
	public void setUrlResolved(String urlResolved)
	{
		this.urlResolved = urlResolved;
	}

	@JsonProperty("homepage")
	public String getHomepage()
	{
		return homepage;
	}

	@JsonProperty("homepage")
	public void setHomepage(String homepage)
	{
		this.homepage = homepage;
	}

	@JsonProperty("favicon")
	public String getFavicon()
	{
		return favicon;
	}

	@JsonProperty("favicon")
	public void setFavicon(String favicon)
	{
		this.favicon = favicon;
	}

	@JsonProperty("tags")
	public String getTags()
	{
		return tags;
	}

	@JsonProperty("tags")
	public void setTags(String tags)
	{
		this.tags = tags;
	}

	@JsonProperty("country")
	public String getCountry()
	{
		return country;
	}

	@JsonProperty("country")
	public void setCountry(String country)
	{
		this.country = country;
	}

	@JsonProperty("countrycode")
	public String getCountrycode()
	{
		return countrycode;
	}

	@JsonProperty("countrycode")
	public void setCountrycode(String countrycode)
	{
		this.countrycode = countrycode;
	}

	@JsonProperty("state")
	public String getState()
	{
		return state;
	}

	@JsonProperty("state")
	public void setState(String state)
	{
		this.state = state;
	}

	@JsonProperty("language")
	public String getLanguage()
	{
		return language;
	}

	@JsonProperty("language")
	public void setLanguage(String language)
	{
		this.language = language;
	}

	@JsonProperty("votes")
	public int getVotes()
	{
		return votes;
	}

	@JsonProperty("votes")
	public void setVotes(int votes)
	{
		this.votes = votes;
	}

	@JsonProperty("lastchangetime")
	public String getLastchangetime()
	{
		return lastchangetime;
	}

	@JsonProperty("lastchangetime")
	public void setLastchangetime(String lastchangetime)
	{
		this.lastchangetime = lastchangetime;
	}

	@JsonProperty("codec")
	public String getCodec()
	{
		return codec;
	}

	@JsonProperty("codec")
	public void setCodec(String codec)
	{
		this.codec = codec;
	}

	@JsonProperty("bitrate")
	public int getBitrate()
	{
		return bitrate;
	}

	@JsonProperty("bitrate")
	public void setBitrate(int bitrate)
	{
		this.bitrate = bitrate;
	}

	@JsonProperty("hls")
	public int getHls()
	{
		return hls;
	}

	@JsonProperty("hls")
	public void setHls(int hls)
	{
		this.hls = hls;
	}

	@JsonProperty("lastcheckok")
	public int getLastcheckok()
	{
		return lastcheckok;
	}

	@JsonProperty("lastcheckok")
	public void setLastcheckok(int lastcheckok)
	{
		this.lastcheckok = lastcheckok;
	}

	@JsonProperty("lastchecktime")
	public String getLastchecktime()
	{
		return lastchecktime;
	}

	@JsonProperty("lastchecktime")
	public void setLastchecktime(String lastchecktime)
	{
		this.lastchecktime = lastchecktime;
	}

	@JsonProperty("lastcheckoktime")
	public String getLastcheckoktime()
	{
		return lastcheckoktime;
	}

	@JsonProperty("lastcheckoktime")
	public void setLastcheckoktime(String lastcheckoktime)
	{
		this.lastcheckoktime = lastcheckoktime;
	}

	@JsonProperty("lastlocalchecktime")
	public String getLastlocalchecktime()
	{
		return lastlocalchecktime;
	}

	@JsonProperty("lastlocalchecktime")
	public void setLastlocalchecktime(String lastlocalchecktime)
	{
		this.lastlocalchecktime = lastlocalchecktime;
	}

	@JsonProperty("clicktimestamp")
	public String getClicktimestamp()
	{
		return clicktimestamp;
	}

	@JsonProperty("clicktimestamp")
	public void setClicktimestamp(String clicktimestamp)
	{
		this.clicktimestamp = clicktimestamp;
	}

	@JsonProperty("clickcount")
	public int getClickcount()
	{
		return clickcount;
	}

	@JsonProperty("clickcount")
	public void setClickcount(int clickcount)
	{
		this.clickcount = clickcount;
	}

	@JsonProperty("clicktrend")
	public int getClicktrend()
	{
		return clicktrend;
	}

	@JsonProperty("clicktrend")
	public void setClicktrend(int clicktrend)
	{
		this.clicktrend = clicktrend;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

}
