package phoenix.player.models;

import javafx.scene.control.TableColumn;

/**
 * Container class which hold the table columns
 * @author skywalker
 *
 */
public class TableColumnCotainer
{
	private final TableColumn<SongModel, Integer> idColumn;
	private final TableColumn<SongModel, String> songNameColumn;
	private final TableColumn<SongModel, String> metaDataColumn;

	public TableColumnCotainer(TableColumn<SongModel, Integer> idColumn,
							   TableColumn<SongModel, String> songNameColumn,
							   TableColumn<SongModel, String> metaDataColumn)
	{
		this.idColumn = idColumn;
		this.songNameColumn = songNameColumn;
		this.metaDataColumn = metaDataColumn;
	}

	public TableColumn<SongModel, Integer> getIdColumn()
	{
		return idColumn;
	}

	public TableColumn<SongModel, String> getSongNameColumn()
	{
		return songNameColumn;
	}

	public TableColumn<SongModel, String> getMetaDataColumn()
	{
		return metaDataColumn;
	}

}
