package phoenix.player.models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;

/**
 * SongModel contains properties of the song like name, path and so on ...
 * @author skywalker
 *
 */
public class SongModel
{
	IntegerProperty idProperty = new SimpleIntegerProperty();
	StringProperty songNamePropery = new SimpleStringProperty();
	StringProperty metaDataProperty = new SimpleStringProperty();

	public SongModel()
	{
	}

	public SongModel(Integer id, String songName, String metaData)
	{
		this.idProperty.set(id.intValue());
		this.songNamePropery.set(songName);
		this.metaDataProperty.set(metaData);
	}

	@SuppressWarnings("rawtypes")
	public ObservableValue getIdProperty()
	{
		return this.idProperty;
	}

	@SuppressWarnings("rawtypes")
	public ObservableValue getSongNamePropery()
	{
		return this.songNamePropery;
	}

	@SuppressWarnings("rawtypes")
	public ObservableValue getMetaDataProperty()
	{
		return this.metaDataProperty;
	}

	public Integer getId()
	{
		return Integer.valueOf(this.idProperty.get());
	}

	public void setId(Integer id)
	{
		this.idProperty.set(id.intValue());
	}

	public String getMetaData()
	{
		return (String) this.metaDataProperty.get();
	}

	public void setMetaData(String metaData)
	{
		this.metaDataProperty.set(metaData);
	}

	public String getSongName()
	{
		return (String) this.songNamePropery.get();
	}

	public void setSongName(String sondName)
	{
		this.songNamePropery.set(sondName);
	}
}
