package phoenix.player.configurators.buttons;

import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;
import phoenix.player.configurators.ComponentConf;
import phoenix.player.models.SongModel;

/**
 * Configure play button action
 * @author skywalker
 *
 */
public class PlayButtonConf implements ComponentConf
{

	private final Button playButton;
	private MediaPlayer mediaPlayer;
	private final SongModel selectedSong;
	private final Slider volumeControl;
	private final Button nextButton;

	public PlayButtonConf(Button playButton, SongModel selectedSong, Slider volumeControl, Button nextButton)
	{
		this.playButton = playButton;
		this.selectedSong = selectedSong;
		this.volumeControl = volumeControl;
		this.nextButton = nextButton;
	}

	@Override
	public void configure()
	{
		playButton.setOnAction((e) ->
		{
			if (null != mediaPlayer)
			{
				if (Status.PAUSED.equals(mediaPlayer.getStatus()))
				{
					mediaPlayer.play();
				} else
				{
					createNewMediaPlayer();
				}
			} else
			{
				createNewMediaPlayer();
			}

		});

	}

	private void createNewMediaPlayer()
	{
		if (null != mediaPlayer)
		{
			mediaPlayer.stop();
		}

		if (null != selectedSong && null != selectedSong.getMetaData())
		{
			String mediaSrc = selectedSong.getMetaData();
			Media media = new Media(mediaSrc);
			mediaPlayer = new MediaPlayer(media);
			volumeControl.valueProperty().addListener((ob, ov, nv) ->
			{
				mediaPlayer.setVolume(((Double) nv).doubleValue() / 100.0D);
			});
			
			// Thread must sleep some time so the mediaPlayer is set to the right state
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//mediaPlayer.play();
			mediaPlayer.setAutoPlay(true);

			mediaPlayer.setOnError(() -> System.out.println("Error with the stream"));
			mediaPlayer.setOnEndOfMedia(() ->
			{
				nextButton.fire();
			});
		}

	}

	public MediaPlayer getMediaPlayer()
	{
		return mediaPlayer;
	}

}
