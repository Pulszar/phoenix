package phoenix.player.configurators.buttons;

import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import phoenix.player.configurators.ComponentConf;
import phoenix.player.models.SongModel;

/**
 * Configure previous button action
 * @author skywalker
 *
 */
public class PreviousButtonConf implements ComponentConf
{
	private Button previousButton;
	private TableView<SongModel> songsTable;
	private Button playButton;

	public PreviousButtonConf(Button previousButton, TableView<SongModel> songsTable, Button playButton)
	{
		this.previousButton = previousButton;
		this.songsTable = songsTable;
		this.playButton = playButton;
	}

	@Override
	public void configure()
	{
		previousButton.setOnAction((e) ->
		{
			songsTable.getSelectionModel().selectPrevious();
			playButton.fire();
		});
	}

}
