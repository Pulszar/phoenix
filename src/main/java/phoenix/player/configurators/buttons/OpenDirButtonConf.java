package phoenix.player.configurators.buttons;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import phoenix.player.configurators.ComponentConf;
import phoenix.player.models.SongModel;

/**
 * Configure open directory button action
 * @author skywalker
 *
 */
public class OpenDirButtonConf implements ComponentConf
{
	private Button openDirectory;
	private ObservableList<SongModel> models;
	private Stage stage;

	public OpenDirButtonConf(Button openDirectory, ObservableList<SongModel> models, Stage stage)
	{
		this.openDirectory = openDirectory;
		this.models = models;
		this.stage = stage;
	}

	public void configure()
	{
		openDirectory.setOnAction((e) ->
		{
			DirectoryChooser directoryChooser = new DirectoryChooser();
			directoryChooser.setTitle("Open directory music");
			File file = directoryChooser.showDialog(stage);
			if (null != file && file.isDirectory())
			{
				models.clear();
				List<File> allMusicFiles = findAllMusicInFolder(file.getAbsolutePath());
				int count = 1;
				Iterator<File> it = allMusicFiles.iterator();

				while (it.hasNext())
				{
					File musicFile = (File) it.next();
					String name = musicFile.getName();
					String parentPath = musicFile.toURI().toString();
					SongModel newModel = new SongModel(Integer.valueOf(count++), name, parentPath);
					models.add(newModel);
				}
			}

		});
	}

	/**
	 * Get all the mp3 files from a folder
	 * 
	 * @param path
	 *            to the folder
	 * @return
	 */
	private static List<File> findAllMusicInFolder(String path)
	{
		List<File> musicList = new ArrayList<>();
		File[] files = (new File(path)).listFiles();

		for (File file : files)
		{
			if (file.isFile())
			{
				String name = file.getName();
				int i = name.lastIndexOf(46);
				if (i > 0)
				{
					String extension = name.substring(i + 1);
					if (extension.equalsIgnoreCase("mp3"))
					{
						musicList.add(file);
					}
				}
			}
		}

		return musicList;
	}
}
