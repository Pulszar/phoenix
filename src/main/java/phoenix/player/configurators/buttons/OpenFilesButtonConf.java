package phoenix.player.configurators.buttons;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import phoenix.player.configurators.ComponentConf;
import phoenix.player.models.SongModel;
import javafx.stage.Stage;

/**
 * Configure open file button action
 * @author skywalker
 *
 */
public class OpenFilesButtonConf implements ComponentConf
{
	private Button openFile;
	private ObservableList<SongModel> models;
	private Stage stage;

	public OpenFilesButtonConf(Button openFile, ObservableList<SongModel> models, Stage stage)
	{
		this.openFile = openFile;
		this.models = models;
		this.stage = stage;
	}

	public void configure()
	{
		openFile.setOnAction((e) ->
		{
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Open music");
			fileChooser.getExtensionFilters().add(new ExtensionFilter("MP3", new String[] { "*.mp3" }));
			models.clear();
			List<File> allMusicFiles = fileChooser.showOpenMultipleDialog(stage);
			if (null != allMusicFiles)
			{
				int count = 1;
				Iterator<File> it = allMusicFiles.iterator();

				while (it.hasNext())
				{
					File musicFile = it.next();
					String name = musicFile.getName();
					String parentPath = musicFile.toURI().toString();
					SongModel newModel = new SongModel(Integer.valueOf(count++), name, parentPath);
					models.add(newModel);
				}
			}
		});
	}
}
