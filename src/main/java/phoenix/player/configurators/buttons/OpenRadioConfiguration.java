package phoenix.player.configurators.buttons;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import phoenix.player.models.RadioModel;
import phoenix.player.models.SongModel;

public class OpenRadioConfiguration
{
	private Button openRadio;
	private ObservableList<SongModel> models;

	public OpenRadioConfiguration(Button openRadio, ObservableList<SongModel> models)
	{
		this.openRadio = openRadio;
		this.models = models;
	}

	public void configure()
	{
		openRadio.setOnAction((e) -> {
			List<String> stations = new ArrayList<>();
			// add all round robin servers one by one to select them separately
			InetAddress[] list;
			try
			{
				list = InetAddress.getAllByName("all.api.radio-browser.info");
				for (InetAddress item : list)
				{
					stations.add(item.getCanonicalHostName());
				}
			} catch (UnknownHostException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (!stations.isEmpty())
			{
				String firstHost = stations.get(0);

				List<RadioModel> radioModels = getStations(firstHost);

				Holder<Integer> runCount = new Holder<>(0);
				radioModels.forEach(rm -> {
					models.add(new SongModel(runCount.getValue(), rm.getName(), rm.getUrl()));
					Integer value = runCount.getValue();
					runCount.setValue(++value);
				});
			}
			stations.forEach(System.out::println);
		});
	}

	private List<RadioModel> getStations(String host)
	{
		List<RadioModel> result = new ArrayList<RadioModel>();
		final HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();

		HttpRequest request = HttpRequest.newBuilder().GET().uri(URI.create("https://" + host + "/json/stations?limit=100"))
				.setHeader("User-Agent", "Java 11 HttpClient Bot").build();

		HttpResponse<String> response;
		try
		{
			response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
			ObjectMapper objectMapper = new ObjectMapper();
			List<RadioModel> radioModels = objectMapper.readValue(response.body(), new TypeReference<List<RadioModel>>(){});
			List<RadioModel> radioMp3Models = radioModels.stream()
												.filter(rm -> rm.getUrl().endsWith("mp3")).collect(Collectors.toList());
			result.addAll(radioMp3Models);
		} catch (IOException | InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
}
