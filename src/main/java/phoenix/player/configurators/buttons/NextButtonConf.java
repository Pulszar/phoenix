package phoenix.player.configurators.buttons;

import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import phoenix.player.configurators.ComponentConf;
import phoenix.player.models.SongModel;

/**
 * Configure next button action
 * @author skywalker
 *
 */
public class NextButtonConf implements ComponentConf
{
	private Button nextButton;
	private Button playButton;
	TableView<SongModel> songsTable;
	ObservableList<SongModel> models;
	
	
	public NextButtonConf(Button nextButton, TableView<SongModel> songsTable, Button playButton, ObservableList<SongModel> models)
	{
		this.nextButton = nextButton;
		this.playButton = playButton;
		this.songsTable = songsTable;
		this.models = models;
	}

	@Override
	public void configure()
	{
		nextButton.setOnAction((e) ->
		{
			if (songsTable.getSelectionModel().isSelected(models.size() - 1))
			{
				songsTable.getSelectionModel().selectFirst();
			} else
			{
				songsTable.getSelectionModel().selectNext();
			}

			playButton.fire();
		});

	}

}
