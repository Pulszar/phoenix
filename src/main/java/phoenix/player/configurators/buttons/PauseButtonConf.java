package phoenix.player.configurators.buttons;

import javafx.scene.control.Button;
import javafx.scene.media.MediaPlayer;
import phoenix.player.configurators.ComponentConf;

/**
 * Configure Pause button action
 * @author skywalker
 *
 */
public class PauseButtonConf implements ComponentConf
{
	private final Button pauseButton;
	private PlayButtonConf playButtonConf;

	public PauseButtonConf(Button pauseButton, PlayButtonConf playButtonConf)
	{
		this.pauseButton = pauseButton;
		this.playButtonConf = playButtonConf;
	}
	
	
	@Override
	public void configure()
	{
		this.pauseButton.setOnAction((e) ->
		{
			MediaPlayer mediaPlayer = playButtonConf.getMediaPlayer();
			if (null != mediaPlayer)
			{
				mediaPlayer.pause();
			}

		});
	}

}
