package phoenix.player.configurators;

import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import phoenix.player.models.SongModel;
import phoenix.player.models.TableColumnCotainer;

/**
 * Table configuration class.
 * @author skywalker
 *
 */
public class TableConf implements ComponentConf
{
	private TableView<SongModel> songsTable;
	private ObservableList<SongModel> models;
	private TableColumnCotainer tableColumnCotainer;
	private SongModel selectedSong;
	private Button playButton;

	public TableConf(TableView<SongModel> songsTable, ObservableList<SongModel> models, TableColumnCotainer tableColumnCotainer, 
			SongModel selectedSong, Button playButton)
	{
		this.songsTable = songsTable;
		this.models = models;
		this.tableColumnCotainer = tableColumnCotainer;
		this.selectedSong = selectedSong;
		this.playButton = playButton;
	}
	

	/**
	 * Add listener to start clicked with mouse song.
	 * @param selectedSong
	 * @param playButton
	 */
	private void addTableRowClickListener(SongModel selectedSong, Button playButton)
	{
		songsTable.setRowFactory(tv ->
		{
			TableRow<SongModel> row = new TableRow<>();
			row.setOnMouseClicked(event ->
			{
				if (event.getClickCount() == 2 && (!row.isEmpty()))
				{
					SongModel rowData = row.getItem();
					selectedSong.setId(rowData.getId());
					selectedSong.setSongName(rowData.getSongName());
					selectedSong.setMetaData(rowData.getMetaData());
					playButton.fire();
				}
			});
			
			return row;
		});
	}

	/**
	 * Add song data to the table columns.
	 * @param idColumn
	 * @param songNameColumn
	 * @param metaDataColumn
	 * @param models
	 */
	@SuppressWarnings("unchecked")
	private void configureTableColumns(TableColumn<SongModel, Integer> idColumn, TableColumn<SongModel, String> songNameColumn,
			TableColumn<SongModel, String> metaDataColumn, ObservableList<SongModel> models)
	{
		Label initialText = new Label("Please add songs with open directory/files buttons!");
		initialText.setId("initialText");
		songsTable.setPlaceholder(initialText);
		idColumn.setCellValueFactory((row) ->
		{
			return ((SongModel) row.getValue()).getIdProperty();
		});
		songNameColumn.setCellValueFactory((row) ->
		{
			return ((SongModel) row.getValue()).getSongNamePropery();
		});
		metaDataColumn.setCellValueFactory((row) ->
		{
			return ((SongModel) row.getValue()).getMetaDataProperty();
		});
		songsTable.setItems(models);
	}

	/**
	 * Update selected song properties.
	 * @param selectedSong
	 */
	private void addUpdateSelectedSongListener(SongModel selectedSong)
	{
		songsTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
		{
			if (null != newValue)
			{
				selectedSong.setId(newValue.getId());
				selectedSong.setSongName(newValue.getSongName());
				selectedSong.setMetaData(newValue.getMetaData());
			} else
			{
				selectedSong.setId(Integer.valueOf(0));
				selectedSong.setSongName("");
				selectedSong.setMetaData("");
			}

		});
		
	}

	@Override
	public void configure()
	{
		configureTableColumns(tableColumnCotainer.getIdColumn(),
				              tableColumnCotainer.getSongNameColumn(),
				              tableColumnCotainer.getMetaDataColumn(),
				              models);
		addTableRowClickListener(selectedSong, playButton);
		addUpdateSelectedSongListener(selectedSong);
	}
}
