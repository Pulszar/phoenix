package phoenix.player.configurators;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * Configure the scene.
 * 
 * Add Key press events to the scene.
 *  Pressing 'N' for next song.
 *  Pressing 'B' for previous song.
 *  Pressing 'Enter' to start current selected song.
 *  Pressing 'P' for pause current song.
 * 
 * @author skywalker
 *
 */
public class SceneConfigurator implements ComponentConf
{
	private Scene scene;
	private Button nextButton;
	private Button previousButton;
	private Button playButton;
	private Button pauseButton;

	public SceneConfigurator(Scene scene, Button nextButton, Button previousButton, Button playButton,
			Button pauseButton)
	{
		this.scene = scene;
		this.nextButton = nextButton;
		this.previousButton = previousButton;
		this.playButton = playButton;
		this.pauseButton = pauseButton;
	}

	public void configure()
	{
		scene.addEventHandler(KeyEvent.KEY_PRESSED, (keyEvent) ->
		{
			if (keyEvent.getCode() == KeyCode.N)
			{
				nextButton.fire();
			}

		});
		scene.addEventHandler(KeyEvent.KEY_PRESSED, (keyEvent) ->
		{
			if (keyEvent.getCode() == KeyCode.B)
			{
				previousButton.fire();
			}

		});
		scene.addEventHandler(KeyEvent.KEY_PRESSED, (keyEvent) ->
		{
			if (keyEvent.getCode() == KeyCode.ENTER)
			{
				playButton.fire();
			}

		});
		scene.addEventHandler(KeyEvent.KEY_PRESSED, (keyEvent) ->
		{
			if (keyEvent.getCode() == KeyCode.P)
			{
				pauseButton.fire();
			}

		});
	}
}
