package phoenix.player;

import java.io.Serializable;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import phoenix.player.configurators.SceneConfigurator;
import phoenix.player.configurators.TableConf;
import phoenix.player.configurators.buttons.NextButtonConf;
import phoenix.player.configurators.buttons.OpenDirButtonConf;
import phoenix.player.configurators.buttons.OpenFilesButtonConf;
import phoenix.player.configurators.buttons.OpenRadioConfiguration;
import phoenix.player.configurators.buttons.PauseButtonConf;
import phoenix.player.configurators.buttons.PlayButtonConf;
import phoenix.player.configurators.buttons.PreviousButtonConf;
import phoenix.player.models.SongModel;
import phoenix.player.models.TableColumnCotainer;

/**
 * In this controller the UI components are configured
 * 
 * @author skywalker
 *
 */
public class StageController implements Serializable, Initializable
{
	private static final long serialVersionUID = 4768794655215537132L;

	private Stage stage;
	private final SongModel selectedSong = new SongModel();
	private final ObservableList<SongModel> models = FXCollections.observableArrayList();
	@FXML
	private TableView<SongModel> songsTable;
	@FXML
	private TableColumn<SongModel, Integer> idColumn;
	@FXML
	private TableColumn<SongModel, String> songNameColumn;
	@FXML
	private TableColumn<SongModel, String> metaDataColumn;
	@FXML
	private Button previousButton;
	@FXML
	private Button playButton;
	@FXML
	private Button pauseButton;
	@FXML
	private Button nextButton;
	@FXML
	private Slider volumeControl;
	@FXML
	private Button openFile;
	@FXML
	private Button openDirectory;
	@FXML
	private Button openRadio;

	/**
	 * This method will be called on application start because StageController implements
	 * Initializable
	 */
	public void initialize(URL location, ResourceBundle resources)
	{
		metaDataColumn.setSortable(false);
		
		PlayButtonConf playButtonConf = new PlayButtonConf(playButton, selectedSong, volumeControl, nextButton);
		playButtonConf.configure();
		
		PauseButtonConf pauseButtonConf = new PauseButtonConf(pauseButton, playButtonConf);
		pauseButtonConf.configure();
		
		NextButtonConf nextButtonConf = new NextButtonConf(nextButton, songsTable, playButton, models);
		nextButtonConf.configure();
		
		PreviousButtonConf previousButtonConf = new PreviousButtonConf(previousButton, songsTable, playButton);
		previousButtonConf.configure();

		OpenDirButtonConf openDirButtonConf = new OpenDirButtonConf(openDirectory, models, stage);
		openDirButtonConf.configure();
		
		OpenFilesButtonConf openFilesButtonConf = new OpenFilesButtonConf(openFile, models, stage);
		openFilesButtonConf.configure();
		
		OpenRadioConfiguration openRadioConf = new OpenRadioConfiguration(openRadio, models);
		openRadioConf.configure();
		
		TableColumnCotainer tableColumnCotainer = new TableColumnCotainer(idColumn, songNameColumn, metaDataColumn);
		
		TableConf tableHandler = new TableConf(songsTable, models, tableColumnCotainer, selectedSong, playButton);
		tableHandler.configure();
	}

	/**
	 * When the scene is ready use this method to configure it.
	 * @param scene
	 */
	void configureScene(Scene scene)
	{
		SceneConfigurator sceneConfigurator = new SceneConfigurator(scene, nextButton, previousButton, playButton, pauseButton);
		sceneConfigurator.configure();
	}
}
