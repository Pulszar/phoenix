package phoenix.player;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Main Class which the application start
 * @author skywalker
 *
 */
public class PhoenixStart extends Application
{
	public static final double APP_WIDTH = 800.0D;
	public static final double APP_HEIGHT = 600.0D;
	public static final String APP_TITLE = "Phoenix Player V4";
	
	public static final String APP_FXML_FILE_PATH = "/stage.fxml";
	public static final String APP_CSS_FILE_PATH = "/stage.css";
	
	public static void main(String[] args)
	{
		launch(args);
	}

	/**
	 * start method will be called because PhoenixStart extends Application
	 */
	public void start(Stage primaryStage)
	{
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource(APP_FXML_FILE_PATH));
		GridPane grid = null;

		Scene scene;
		try
		{
			grid = (GridPane) loader.load();
			scene = new Scene(grid, APP_WIDTH, APP_HEIGHT);
		} catch (IOException err)
		{
			Label errorLabel = new Label("Error the Main component was not loaded. This is scary!");
			scene = new Scene(errorLabel, APP_WIDTH, APP_HEIGHT);
			err.printStackTrace();
		}

		String cssFile = this.getClass().getResource(APP_CSS_FILE_PATH).toString();
		scene.getStylesheets().add(cssFile);
		StageController controller = (StageController) loader.getController();
		controller.configureScene(scene);
		primaryStage.setTitle(APP_TITLE);
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}
